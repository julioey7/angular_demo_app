import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, EMPTY, throwError, of } from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
    constructor(private toastr: ToastrService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError((err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
                console.log('An error ocurred >>', err.error.message);
                this.toastr.error('Por favor vuelve a intentarlo mas tarde', 'Ha ocurrido un error', { closeButton: true });
            } else {
                console.error(`Backend returned code ${err.status}, body: ${err.error}`);
                this.toastr.error(err.error, 'Ups!', { closeButton: true });
            }

            return EMPTY;
        }));
    }
}
