import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: 'my-token' }),
        observe: 'response'
    };

    constructor(private http: HttpClient) { }

    get(url, auth) {
        return this.http.get(`${environment.base_url}${url}`, {observe: 'response'}).toPromise();
    }

    post(url, auth, data, body) {
        console.log(environment.base_url);
        return this.http.post(`${environment.base_url}${url}`, body, {observe: 'response'}).toPromise();
    }
}
