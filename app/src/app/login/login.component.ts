import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ValidateService} from '../validate.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../assets/css/form.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor( private formBuilder: FormBuilder, private validateService: ValidateService ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.validateService.isEmail)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }
  submit() {
    console.log('Login >>>', this.loginForm.value);
  }
  get form() { return this.loginForm.controls; }

}
