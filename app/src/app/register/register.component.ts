import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidateService } from '../validate.service';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../../assets/css/form.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  constructor( private formBuilder: FormBuilder, private validateService: ValidateService, private httpService: HttpService ) { }
  ngOnInit() {
      this.registerForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(4)]],
        email: ['', [Validators.required, Validators.pattern(this.validateService.isEmail)]],
        password: ['', [
            Validators.required,
            Validators.minLength(8),
            Validators.pattern(this.validateService.hasSpecialChar),
            Validators.pattern(this.validateService.hasLetters),
            Validators.pattern(this.validateService.hasNumbers)
        ]]
      });
  }
  async submit() {
    console.log('Register >>>', this.registerForm.value);
    const response = await this.httpService.post('security/register', false, this.registerForm.value, null);
    console.log('Response >>>', response);
  }

  get form() { return this.registerForm.controls; }
  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
