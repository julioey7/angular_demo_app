import { Component, OnInit } from '@angular/core';
import { faUserSecret} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.css']
})
export class SecurityComponent implements OnInit {

  faUserSecret = faUserSecret;

  constructor() { }

  ngOnInit() {
  }

}
