import { Component, OnInit } from '@angular/core';
import { faSignInAlt, faShare, faMedal, faMapMarkerAlt, faEnvelope, faCompass } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    faSignInAlt = faSignInAlt;
    faShare = faShare;
    faMedal = faMedal;
    faMapMarkerAlt = faMapMarkerAlt;
    faEnvelope = faEnvelope;
    faCompass = faCompass;
    faFacebookF = faFacebookF;
    faTwitter = faTwitter;
    faInstagram = faInstagram;
    instructions = 1;

    constructor() { }

    ngOnInit() { }
}
